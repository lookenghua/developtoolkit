const {remote, clipboard} = require('electron');
const {Menu, MenuItem} = remote;
export default {
	bind: (el, binding, vnode) => {
		el.oncontextmenu = () => {
			let template = [
				{
					label: '撤销(U)',
					accelerator: 'CmdOrCtrl+Z',
					role: 'undo'
				}, {
					label: '重做(R)',
					accelerator: 'Shift+CmdOrCtrl+Z',
					role: 'redo'
				}, {
					label: '全选(A)',
					accelerator: 'CmdOrCtrl+A',
					role: 'selectall'
				}, {
					type: 'separator'
				}, {
					label: '剪切(T)',
					accelerator: 'CmdOrCtrl+X',
					role: 'cut'
				}, {
					label: '复制(C)',
					accelerator: 'CmdOrCtrl+C',
					role: 'copy'
				}, {
					label: '粘贴(P)',
					accelerator: 'CmdOrCtrl+V',
					role: "paste"
				}, {
					label: '粘贴为纯文本',
					accelerator: 'Shift+CmdOrCtrl+V'
				}];
			const menu = Menu.buildFromTemplate(template);
			menu.popup({window: remote.getCurrentWindow()});
		};
	}
};
