import context from './context';
import preview from './preview';
import hoverimg from './hoverimg';
import copy from './copy';

export default {
	install: function (Vue) {
		Vue.directive('context', context);//右键菜单
		Vue.directive('preview', preview);//图片预览
		Vue.directive('hoverimg', hoverimg);//图片悬浮预览
		Vue.directive('copy', copy);//点击复制
	}
};
