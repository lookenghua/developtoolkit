import Datastore from 'nedb';
import path from 'path';
import {remote} from 'electron';

class Nedb {
	constructor() {
		this.db = {
			imgUpload: new Datastore(path.join(remote.app.getPath('userData'), '/developtoolkit/imgUpload.db')),
			imgUploadHistory: new Datastore(path.join(remote.app.getPath('userData'), '/developtoolkit/imgUploadHistory.db'))
		};
		this.db.imgUpload.loadDatabase();
		this.db.imgUploadHistory.loadDatabase();
	}

	add(dbName, doc) {
		return new Promise((resolve, reject) => {
			this.db[dbName].insert(doc, function (err, newDoc) {   // Callback is optional
				if (err) {
					return reject(err);
				}
				resolve(newDoc);
			});
		});
	}

	delete(dbName, doc) {
		return new Promise((resolve, reject) => {
			this.db[dbName].remove(doc, {multi: true}, function (err, numRemoved) {
				if (err) {
					return reject(err);
				}
				resolve(numRemoved);
			});
		});
	}

	update(dbName, doc, updateDoc) {
		return new Promise((resolve, reject) => {
			this.db[dbName].update(doc, updateDoc, {}, function (err, numReplaced) {
				if (err) {
					return reject(err);
				}
				resolve(numReplaced);
			});
		});

	}

	find(dbName, doc) {
		return new Promise((resolve, reject) => {
			this.db[dbName].find(doc).sort({
				_id: -1
			}).exec((err, ret) => {
				if (err) {
					return reject(err);
				}
				resolve(ret);
			});
		});
	}

	count(dbName, doc) {
		return new Promise((resolve, reject) => {
			this.db[dbName].count(doc, function (err, count) {
				if (err) {
					return reject(err);
				}
				resolve(count);
			});
		});
	}
}

export default new Nedb();
