import Vue from 'vue';
import axios from 'axios';

import App from './App';
import router from './router';
import store from './store';
import filter from './filter';
import directive from './directive';
import db from './datastore';


import iView from 'iview';
import 'iview/dist/styles/iview.css';
import './assets/css/global.styl';

import {initConfig} from './assets/js/init';

Vue.use(iView);
Vue.use(filter);

Vue.use(directive);

initConfig();

if (!process.env.IS_WEB) Vue.use(require('vue-electron'));
Vue.http = Vue.prototype.$http = axios;
Vue.db = Vue.prototype.$db = db;
Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
	components: {App},
	router,
	store,
	template: '<App/>'
}).$mount('#app');
