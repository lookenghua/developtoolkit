const OSS = require('ali-oss');
const request = require('request');

//创建控制台
function createClient(config) {
	let client;
	if (config.bucket) {
		client = new OSS({
			accessKeyId: config.id,
			accessKeySecret: config.secret,
			bucket: config.bucket
		});
	} else {
		client = new OSS({
			accessKeyId: config.id,
			accessKeySecret: config.secret,
			bucket: config.bucket
		});
	}

	return client;
}

//列出所有bucket
export const listBuckets = async (config) => {
	let client = createClient(config);
	try {
		let result = await client.listBuckets();
		return result;
	} catch (err) {
		return err;
	}
};

export const uploadToAlioss = async (config, file) => {
	console.log(config.id, config.secret, config.bucket);
	let client = createClient(config);
	let fileName = new Date().getTime().toString();
	console.log(fileName);
	try {
		let result = await client.put(fileName, file);
		return result;
	} catch (e) {
		return e;
	}
};
