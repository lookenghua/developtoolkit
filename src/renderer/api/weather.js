const request = require('request');
import MD5 from 'md5';

const qs = require('qs');
import baiduConfig from '../config/baiduConfig';
import weatherConfig from '../config/heweatherConfig';

//排序
function alphabeticalSort (a, b) {
  return a.localeCompare(b);
}

//生成sign
const createSign = (parame) => {
  let obj;
  obj = qs.stringify(parame, {sort: alphabeticalSort}) + weatherConfig.key;
  return MD5(obj);
};
//获取城市信息
export const getCity = () => {
  return new Promise((resolve, reject) => {
    const parame = qs.stringify({
      'ak': baiduConfig.AK,
      'coor': 'gcj02'
    });
    request(`https://api.map.baidu.com/location/ip?${parame}`, {json: true}, (error, response, body) => {
      if (error) return reject(error);
      if (!error && response.statusCode == 200) {
        resolve(body);
      }
    });
  });
};
//获得城市编码
export const getCityCode = (cityName) => {
  return new Promise((resolve, reject) => {
    const parame = qs.stringify({
      location: cityName,
      key: weatherConfig.key
    });
    request(`https://search.heweather.com/find?${parame}`, {json: true}, (error, response, body) => {
      if (error) return reject(error);
      if (!error && response.statusCode == 200) {
        resolve(body);
      }
    });
  });
};
//获取3-10天天气预报
export const weatherForecast = (location) => {
  return new Promise((resolve, reject) => {
    const baseUrl = 'https://free-api.heweather.com/s6/weather/forecast';
    let parame = qs.stringify({
      location: location,
      key: weatherConfig.key
    });
    request(`${baseUrl}?${parame}`, {json: true}, (error, response, body) => {
      if (error) return reject(error);
      if (!error && response.statusCode == 200) {
        resolve(body);
      }
    });
  });
  
};
//实况天气
export const weatherNow = (location) => {
  return new Promise((resolve, reject) => {
    const baseUrl = 'https://free-api.heweather.com/s6/weather/now';
    let parame = qs.stringify({
      location: location,
      key: weatherConfig.key
    });
    request(`${baseUrl}?${parame}`, {json: true}, (error, response, body) => {
      if (error) return reject(error);
      if (!error && response.statusCode == 200) {
        resolve(body);
      }
    });
  });
};
//逐小时预报
export const weatherHourly = (location) => {
  return new Promise((resolve, reject) => {
    const baseUrl = 'https://free-api.heweather.com/s6/weather/hourly';
    let parame = qs.stringify({
      location: location,
      key: weatherConfig.key
    });
    request(`${baseUrl}?${parame}`, {json: true}, (error, response, body) => {
      if (error) return reject(error);
      if (!error && response.statusCode == 200) {
        resolve(body);
      }
    });
  });
};
