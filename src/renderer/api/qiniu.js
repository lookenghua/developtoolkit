import qiniu from 'qiniu';

const request = require('request');
let utils = qiniu.util;

//创建mac
const createMAC = (config) => {
  let accessKey = config.AK;
  let secretKey = config.SK;
  let mac = new qiniu.auth.digest.Mac(accessKey, secretKey);
  return mac;
};

//获取管理凭证
function getQboxToken(config, url) {
  let token = utils.generateAccessToken(createMAC(config), url);
  return token;
}

//创建获取Bucket
export const getBuckets = (config) => {
  return new Promise((resolve, reject) => {
    let url = 'http://rs.qbox.me/buckets';
    let token = getQboxToken(config, url);
    request('http://rs.qbox.me/buckets', {
      headers: {
        'content-type': 'application/x-www-form-urlencoded',
        'Authorization': token
      },
			json:true
    }, (error, response, body) => {
      if (error) {
        return reject(error);
      }
      resolve(body);
    });
  });
};

//服务器上传
export const uploadToqiniu = (Config, path) => {
  return new Promise((resolve, reject) => {
    let mac = createMAC(Config);
    const options = {
      scope: Config.bucket
    };
    const putPolicy = new qiniu.rs.PutPolicy(options);
    const uploadToken = putPolicy.uploadToken(mac);
    const config = new qiniu.conf.Config();
    const localFile = path;
    const formUploader = new qiniu.form_up.FormUploader(config);
    const putExtra = new qiniu.form_up.PutExtra();
    const key = new Date().getTime();
    // 文件上传
    formUploader.putFile(uploadToken, key, localFile, putExtra, function (respErr, respBody, respInfo) {
      if (respErr) {
        console.log(respErr);
        reject(respBody);
      }
      if (respInfo && respInfo.statusCode == 200) {
        resolve(respBody);
      } else {
        reject(respInfo);
      }
    });
  });

};

//获取 Bucket 空间域名
export const getBucketTbl = (config) => {
  return new Promise((resolve, reject) => {
    let url = `http://api.qiniu.com/v6/domain/list?tbl=${config.bucket}`;
    let token = getQboxToken(config, url);
    request(url, {
      headers: {
        'content-type': 'application/x-www-form-urlencoded',
        'Authorization': token
      },
      json: true
    }, (error, response, body) => {
      if (error) {
        return reject(error);
      }
      resolve(body);
    });
  });
};


