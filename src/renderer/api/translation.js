const request = require('request');
const qs = require('qs');
import MD5 from 'md5';
import youdaoConfig from '@/config/youdaoConfig';
import baiduConfig from '@/config/baidutranslateConfig';

export const youdaoTranslation = (from, to, query) => {
  return new Promise((resolve, reject) => {
    const appKey = youdaoConfig.appKey;
    const key = youdaoConfig.key;
    const salt = new Date().getTime();
    const sign = MD5(appKey + query + salt + key);
    const parame = qs.stringify({
      "q": query,
      "appKey": appKey,
      "salt": salt,
      "from": from,
      "to": to,
      "sign": sign
    });
    request(`http://openapi.youdao.com/api?${parame}`, {json: true}, (error, response, body) => {
      if (error) return reject(error);
      if (!error && response.statusCode == 200) {
        resolve(body);
      }
    });
  });

};
export const baiduTranslation = (from, to, query) => {
  return new Promise((resolve, reject) => {
    const appid = baiduConfig.APP_ID;
    const appKey = baiduConfig.secret;
    const salt = new Date().getTime();
    const sign = MD5(appid + query + salt + appKey).toLowerCase();
    const parame = qs.stringify({
      "q": query,
      "appKey": appKey,
      "salt": salt,
      "from": from,
      "to": to,
      "sign": sign,
      "appid": appid
    });
    request(`https://fanyi-api.baidu.com/api/trans/vip/translate?${parame}`, {json: true}, (error, response, body) => {
      if (error) return reject(error);
      if (!error && response.statusCode == 200) {
        resolve(body);
      }
    });
  });

};
