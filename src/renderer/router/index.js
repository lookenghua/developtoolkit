import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
	routes: [
		{
			path: '/',
			name: 'index',
			component: require('@/views/index').default
		}, {
			path: '/convert/texttransform',
			component: require('@/views/convert/textTransform').default
		}, {
			path: '/convert/timetransform',
			component: require('@/views/convert/timeTransform').default
		}, {
			path: '/covert/color',
			component: require('@/views/convert/colorConvert').default
		}, {
			path: '/encrypt/md5',
			component: require('@/views/encrypt/md5Encrypt').default
		}, {
			path: '/encrypt/base64',
			component: require('@/views/encrypt/base64Encrypt').default
		}, {
			path: '/format/html',
			component: require('@/views/format/html-format').default
		}, {
			path: '/format/json',
			component: require('@/views/format/jsonFormat').default
		}, {
			path: '/tools/qrcode',
			name:'qrcode',
			component: require('@/views/tools/qrcode').default
		}, {
			path: '/tools/http',
			component: require('@/views/tools/httpTool').default
		}, {
			path: '/fromend/ide',
			component: require('@/views/fromEnd/offline_ide').default
		}, {
			path: '/service/translation',
			component: require('@/views/service/textTranslation').default
		}, {
			path: '/life/weather',
			component: require('@/views/life/weather').default
		}, {
			path: '/imgupload/qiniu',
			component: require('@/views/imgUpload/qiniu').default
		}, {
			path: '/imgupload/ali_oss',
			component: require('@/views/imgUpload/ali_oss').default
		}, {
			path: "/imgupload/history",
			name:'HistoryUpload',
			component: require('@/views/imgUpload/HistoryUpload').default
		}, {
			path: '*',
			redirect: '/'
		}]
});
