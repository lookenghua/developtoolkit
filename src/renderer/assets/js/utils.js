import { remote } from 'electron';
import Vue from 'vue';

const path = require('path');
const fs = require('fs');
//新建缓存文件夹
const mkCacheDir = () => {
	let cacheDir = path.join(remote.app.getPath('userData'), `/developtoolkit/cache`);
	fs.existsSync(cacheDir) == false && mkdirs(cacheDir);
};

//新建文件夹
function mkdirs (dirpath) {
	if (!fs.existsSync(path.dirname(dirpath))) {
		mkdirs(path.dirname(dirpath));
	}
	fs.mkdirSync(dirpath);
}

//保存base文件到本地
export const saveBase64ToDesk = async (imgData) => {
	return new Promise((resolve, reject) => {
		const base64Data = imgData.replace(/^data:image\/\w+;base64,/, '');
		const dataBuffer = new Buffer(base64Data, 'base64');
		const fileName = path.join(remote.app.getPath('userData'), `/developtoolkit/cache/${new Date().getTime()}.png`);
		mkCacheDir();
		fs.writeFile(fileName, dataBuffer, function (err) {
			if (err) {
				reject(err);
			} else {
				resolve(fileName);
			}
		});
	});
};

//过滤所有空格
function removeAllSpace (str) {
	return str.replace(/\s+/g, '');
}

//rgba转化成16进制
export function rgbaToHex (text) {
	if (text.match(/\((\S*)\)/)) {
		let str = removeAllSpace(text).match(/\((\S*)\)/)[1];
		let arr = str.split(',');
		let newArr = arr.map(item => {
			if (item.indexOf('.') == 0) {
				return parseInt(Number('0' + item) * 255).toString(16);
			} else if (item.indexOf('.') != -1) {
				return parseInt(item * 255).toString(16);
			} else {
				return parseInt(item).toString(16);
			}
		});
		return '#' + newArr.join('');
	}
}

function Xreplace (str, length, reversed) {
	let re = new RegExp('\\w{1,' + length + '}', 'g');
	let ma = str.match(re);
	if (reversed) ma.reverse();
	return ma.join(',');//最后面不要"a" 就去掉( + "a")
}

//16进制转rgba(a)
export function hexToRgba (text) {
	text = removeAllSpace(text);
	let arr = text.split('#');
	console.log(arr);
	if (arr.length >= 2) {
		if (arr[1].length == 3) {
			arr[1] = Xreplace(arr[1], 1);
		} else {
			arr[1] = Xreplace(arr[1], 2);
		}
	}
	console.log(arr[1]);
	let newArr = arr[1].split(',');
	newArr = newArr.map(item => {
		if (item.length == 1) {
			return item.repeat(2);
		} else {
			return item;
		}
	});
	let str = '';
	if (newArr.length == 4) {
		newArr = newArr.map((item, index) => {
			if (index == 3) {
				return Number('0x' + item) / 255;
			} else {
				return Number('0x' + item);
			}
		});
		str = `rgba(${newArr.join(',')})`;
	} else {
		newArr = newArr.map((item) => {
			return Number('0x' + item);
		});
		str = `rgb(${newArr.join(',')})`;
	}
	return str;
}

//RGBA转HSL
export function rgbaToHsl (text) {
	if (text.match(/\((\S*)\)/)) {
		let str = removeAllSpace(text).match(/\((\S*)\)/)[1];
		let arr = str.split(',');
		let [r, g, b, a] = arr;
		let max = Math.max(r, g, b), min = Math.min(r, g, b);
		let h, s, l = (max + min) / 2;
		
		if (max == min) {
			h = s = 0; // achromatic
		} else {
			let d = max - min;
			s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
			switch (max) {
				case r:
					h = (g - b) / d + (g < b ? 6 : 0);
					break;
				case g:
					h = (b - r) / d + 2;
					break;
				case b:
					h = (r - g) / d + 4;
					break;
			}
			h /= 6;
		}
		
		if (a) {
			return `hsla(${h},${s},${l},${a})`;
		} else {
			return `hsl(${h},${s},${l})`;
		}
	}
	
}

//HSL转RGBA
export function hslaToRgba (text) {
	if (text.match(/\((\S*)\)/)) {
		let str = removeAllSpace(text).match(/\((\S*)\)/)[1];
		let arr = str.split(',');
		let [h, s, l, a] = arr;
		let r, g, b;
		if (s == 0) {
			r = g = b = l; // achromatic
		} else {
			let hue2rgb = (p, q, t) => {
				if (t < 0) t += 1;
				if (t > 1) t -= 1;
				if (t < 1 / 6) return p + (q - p) * 6 * t;
				if (t < 1 / 2) return q;
				if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
				return p;
			};
			
			let q = l < 0.5 ? l * (1 + s) : l + s - l * s;
			let p = 2 * l - q;
			r = hue2rgb(p, q, h + 1 / 3);
			g = hue2rgb(p, q, h);
			b = hue2rgb(p, q, h - 1 / 3);
		}
		if (a) {
			return `rgba(${Math.round(r * 255)},${Math.round(g * 255)},${Math.round(b * 255)},${a})`;
		} else {
			return `rgba(${Math.round(r * 255)},${Math.round(g * 255)},${Math.round(b * 255)})`;
		}
	}
}

//图片转换成base64
export function imgToBase64 (filePath) {
	const imageBuf = fs.readFileSync(filePath);
	return imageBuf.toString('base64');
}

export function copyTextToClipboard (text) {
	return new Promise((resolve, reject) => {
		navigator.clipboard.writeText(text).then(() => {
			Vue.prototype.$Message.success('文本已经成功复制到剪切板');
			resolve();
		}).catch(err => {
			Vue.prototype.$Message.error(`无法复制此文本：${err}`);
			reject(err);
		});
	});
	
}
