const uuidv4 = require('uuid/v4');
const Store = require('electron-store');
const store = new Store();

export function initConfig() {
	if (!store.has('uuid')) {
		store.set('uuid', uuidv4);
	}
}
