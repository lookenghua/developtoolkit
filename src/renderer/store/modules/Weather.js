const state = {
  weather_hourly: {},
  city: '',
  cityCode: '',
  weather_now: {}
};


const mutations = {
  updateCity(state, city) {
    state.city = city;
  },
  updateCityCode(state, cityCode) {
    state.cityCode = cityCode;
  },
  updateWeatherNow(state, data) {
    state.weather_now = data;
  }
};

const actions = {
  someAsyncTask({commit}) {

  }
};

export default {
  state,
  mutations,
  actions
};
