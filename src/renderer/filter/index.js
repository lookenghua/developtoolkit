//格式化时间YYYY-MM-DD mm:ss
const timeFormat = (value) => {
  let date = new Date(value.length == 10 ? value * 1000 : value); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
  const change = (t) => {
    if (t < 10) {
      return "0" + t;
    } else {
      return t;
    }
  };
  let Y = date.getFullYear() + '-';
  let M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
  let D = change(date.getDate()) + ' ';
  let h = change(date.getHours()) + ':';
  let m = change(date.getMinutes()) + ':';
  let s = change(date.getSeconds());
  return Y + M + D + h + m + s;
};
//判断变量类型
const variableType = (variable) => {
  let type = "unknown";
  switch (Object.prototype.toString.call(variable)) {
    case '[object String]':
      type = "string";
      break;
    case "[object Number]":
      type = "number";
      break;
    case "[object Boolean]":
      type = "boolean";
      break;
    case "[object Function]":
      type = "function";
      break;
    case "[object Array]":
      type = "array";
      break;
    case "[object Date]":
      type = "date";
      break;
    case "[object RegExp]":
      type = "regexp";
      break;
    case "[object Undefined]":
      type = "undefined";
      break;
    case "[object Null]":
      type = "null";
      break;
    case "[object Object]":
      type = "object";
      break;
  }
  return type;
};
//判断元素是否存在
const isExist = (value) => {
  let type = variableType(value);
  let bol = true;
  if (type == "string") {
    if (value.length == 0) {
      bol = false;
    }
  } else if (type == "array") {
    if (value.length == 0) {
      bol = false;
    }
  } else if (type == "object") {
    if (Object.keys().length == 0) {
      bol = false;
    }
  } else if (type == "undefined") {
    bol = false;
  } else if (type == "null") {
    bol = false;
  }
  return bol;
};
const percent = (value) => {
  return (value * 100).toFixed(2) + "%";
};
const Filter = {
  install: function (Vue) {
    Vue.filter('time', timeFormat);
    Vue.filter('exist', isExist);
    Vue.filter('percent', percent);
  }
};

export default Filter;
