const Koa = require('koa');
const app = new Koa();
const Static = require('koa-static');
const path = require('path');
const router = require('koa-router')();

app.use(Static(__dirname + '/public'));

export default app;