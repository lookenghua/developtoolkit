import {BrowserWindow, ipcMain, powerSaveBlocker} from 'electron';
import {createTray} from './Tray';

const winURL = process.env.NODE_ENV === 'development' ? `http://localhost:9080` : `file://${__dirname}/index.html`;

let mainWindow = null;
let powerSaveId = null;
const createWindow = () => {
	/**
	 * Initial window options
	 */
	mainWindow = new BrowserWindow({
		height: 563,
		useContentSize: true,
		width: 1000,
		minWidth: 1000,
		minHeight: 563,
		frame: false,
		show: false
	});

	mainWindow.loadURL(winURL);

	mainWindow.on('closed', () => {
		mainWindow = null;
	});
	mainWindow.once('ready-to-show', () => {
		mainWindow.show();
		createTray();
	});

	//监听操作系统
	ipcMain.on('system-change', (event, args) => {
		if (args.type == 1) {
			mainWindow.minimize();
		} else if (args.type == 2) {
			if (mainWindow.isMaximized()) {
				mainWindow.unmaximize();
			} else {
				mainWindow.maximize();
			}
		} else if (args.type == 3) {
			mainWindow.close();
		} else if (args.type == 4) {
			if (powerSaveId == null) {
				powerSaveId = powerSaveBlocker.start('prevent-display-sleep');
			}
		}
	});
};

export {mainWindow, createWindow};
