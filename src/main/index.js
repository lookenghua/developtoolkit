import {app} from 'electron';
import {createWindow, mainWindow} from './mainWindow';
import {autoUpdater} from 'electron-updater';

if (process.env.NODE_ENV !== 'development') {
	global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\');
}
app.commandLine.appendSwitch('--disable-http-cache');
const gotTheLock = app.requestSingleInstanceLock();
if (!gotTheLock) {
	app.quit();
} else {
	app.on('second-instance', (event, commandLine, workingDirectory) => {
		if (mainWindow) {
			if (mainWindow.isMinimized()) mainWindow.restore();
			mainWindow.focus();
		}
	});

	app.on('ready', () => {
		createWindow();
		if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates();
	});
}

app.on('window-all-closed', () => {
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

app.on('activate', () => {
	if (mainWindow === null) {
		createWindow();
	}
});

function updateHandle() {

	let message = {

		error: '检查更新出错',

		checking: '正在检查更新……',

		updateAva: '检测到新版本，正在下载……',

		updateNotAva: '现在使用的就是最新版本，不用更新',

	};

	const os = require('os');

	const uploadUrl = "https://bitbucket.org/lookenghua/developtoolkit/downloads/"; // 下载地址，不加后面的.exe

	autoUpdater.setFeedURL(uploadUrl);

	autoUpdater.on('error', function (error) {


	});

	autoUpdater.on('checking-for-update', function () {
		console.log('checking-for-update');
	});

	autoUpdater.on('update-available', function (info) {


	});

	autoUpdater.on('update-not-available', function (info) {


	});

// 更新下载进度事件

	autoUpdater.on('download-progress', function (progressObj) {

		mainWindow.webContents.send('downloadProgress', progressObj);

	});

	autoUpdater.on('update-downloaded', function (event, releaseNotes, releaseName, releaseDate, updateUrl, quitAndUpdate) {


	});
}


