const {Menu, Tray, BrowserWindow, BrowserView} = require('electron');
import { mainWindow } from './mainWindow';

const path = require('path');

let tray = null;
export const createTray = () => {
	tray = new Tray(path.join(__static, '/icon.ico'));
	const contextMenu = Menu.buildFromTemplate([
		{
			label: '实验性功能',
			submenu: [
				{
					label: 'JSON格式化',
					click () {
						let win = new BrowserWindow({
							title: 'JSON格式化',
							webPreferences: {
								devTools: false
							}
						});
						win.loadURL(process.env.NODE_ENV === 'development' ? `http://localhost:9080/static/jsonon/index.html` : `file://${__dirname}/static/jsonon/index.html`);
						win.on('closed', () => {
							win = null;
						});
						win.once('ready-to-show', () => {
							win.show();
						});
					}
				}]
		},
		{
			label: '关于软件',
			click () {
				console.log();
				let size = mainWindow.getSize();
				let width = 300;
				let height = 300;
				let view = new BrowserView({
					webPreferences: {
						nodeIntegration: false
					}
				});
				mainWindow.setBrowserView(view);
				view.setBounds({
					x: (size[0] - width) / 2,
					y: (size[1] - height) / 2,
					width: width,
					height: height
				});
				view.webContents.loadURL(path.join(__static, '/systemInfo.html'));
			}
		},
		{
			label: '退出软件',
			role: 'quit'
		}]);
	tray.setToolTip('开发工具箱 designed by 朱磊');
	tray.setContextMenu(contextMenu);
	tray.on('click', () => {
		if (mainWindow == null) return;
		mainWindow.isVisible() ? mainWindow.hide() : mainWindow.show();
	});
};



